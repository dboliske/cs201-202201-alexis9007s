// name: Yuanqin Song
// cs201, sec (03), Feb 5, Exercise 1

package labs.lab3;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class ReadInFile {
    public static void main(String[] args) throws IOException {
        File f = new File("src/labs/lab3/grades.csv"); //create a file object
        Scanner input = new Scanner(f);

        double[] grades = new double[2];
        int count = 0;
        double total = 0;
        while (input.hasNext()) {
            String[] studentInfo = input.nextLine().split(",");

            // update the size of array
            if (count == grades.length) {
                double[] resize = new double[grades.length*2];
                for (int i=0; i<count; i++) {
                    resize[i] = grades[i];
                }
                grades = resize;
                resize = null;
            }
            grades[count] = Integer.parseInt(studentInfo[1]);
            total += grades[count];
            count += 1;
        }

        // trim down
        double[] trim = new double[count];
        for (int i=0; i<count; i++) {
            trim[i] = grades[i];
        }
        grades = trim;
        trim = null;

        // compute average grade
        double avg = total/grades.length;
        System.out.println("The average is: " + avg);

        input.close();
    }
}
