// name: Yuanqin Song
// cs201, sec (03), Feb 5, Exercise 2

package labs.lab3;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;


public class WriteToFile {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        String[] numbers = new String[2];
        boolean finished = false;// initialise control flag

        System.out.println("enter \"Done\" to finish inputting.");

        int count = 0;
        String fileName = "src/labs/lab3/";
        do {
            System.out.print("input a number: ");
            String inputting = input.nextLine();

            if (inputting.equals("Done")) {
                System.out.print("input a name for your file: ");
                fileName += input.nextLine();
                finished = true;
            } else {
                // resize
                if (count == numbers.length) {
                    String[] resize = new String[numbers.length*2];
                    for (int i=0; i< numbers.length; i++) {
                        resize[i] = numbers[i];
                    }
                    numbers = resize;
                    resize = null;
                }
                numbers[count] = inputting; // assign inputting to array
                count += 1;
            }
        } while (!finished);

        // trim
        String[] trim = new String[count];
        for (int i=0; i<trim.length; i++) {
            trim[i] = numbers[i];
        }
        numbers = trim;
        trim = null;

        // write data to file
        File myObj = new File(fileName);
        try {
            if (myObj.createNewFile()) {
                FileWriter myWritter = new FileWriter(myObj);
                for (int i=0; i<count; i++) {
                    myWritter.write(numbers[i]+",");
                }
                myWritter.flush();
            } else {System.out.print("Create File Failed!!!");}
        } catch (IOException e) {
            System.out.println("An error occurred: " + e.getMessage());
        }

        input.close();
        System.out.println("GoodBye");
    }
}
