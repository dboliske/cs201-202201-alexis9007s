// name: Yuanqin Song
// cs201, sec (03), Mar 17, DeliCounterClient

package labs.lab6;

import java.util.ArrayList;
import java.util.Scanner;

public class DeliCounterClient {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        ArrayList<String> queue = new ArrayList<String>();
        menu(input,queue);
    }

    public static void menu (Scanner input, ArrayList<String> queue) {
        String[] options = {"Add customer to queue", "Help customer", "Exit"};
        boolean done = false;

        do {
            for (int i=0; i<options.length; i++) {
                System.out.println((i+1) + ". " + options[i]);
            }

            System.out.print("Enter choice: ");
            String choice = input.nextLine();

            switch (choice) {
                case "1":
                    inQueue(input, queue);
                    break;
                case "2":
                    deQueue(queue);
                    break;
                case "3":
                    done = true;
                    System.out.println("Goodbye");
                    break;
                default:
                    System.out.println("Invalid input.");
            }

        } while (!done);

    }

    // Add Customer to Queue
    public static void inQueue (Scanner input, ArrayList<String> queue) {
        System.out.print("Enter your name please: ");
        String name = input.nextLine();

        queue.add(name);
        System.out.println("Customer position: " + (queue.size()-1) );
        System.out.println();
    }

    // Help customer
    public static void deQueue (ArrayList<String> queue) {
        if (queue.size()>0) {
            String frontCustomer = queue.get(0);
            queue.remove(0);    // remove front customer
            System.out.println("The front customer is: " + frontCustomer);
        } else {
            System.out.println("The queue is empty!! The front customer not found.");
        }

        System.out.println();
    }

}
