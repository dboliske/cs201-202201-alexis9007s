// name: Yuanqin Song
// cs201, sec (03), Feb 18, Exercise 2

package labs.lab4;

public class PhoneNumberClient {
    public static void main(String[] args) {
        PhoneNumber pn1 = new PhoneNumber();
        PhoneNumber pn2 = new PhoneNumber("289", "415", "7886399");

        System.out.println(pn1.toString());
        System.out.println(pn2.toString());
    }
}
