// name: Yuanqin Song
// cs201, sec (03), Feb 18, Exercise 3

package labs.lab4;


public class PotionClient {
    public static void main(String[] args) {
        Potion defaultPotion = new Potion();
        Potion ageingPotion =new Potion("Aging Potion", 8);

        System.out.println(defaultPotion.toString());
        System.out.println(ageingPotion.toString());


    }
}
