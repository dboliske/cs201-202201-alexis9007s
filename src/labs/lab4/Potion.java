// name: Yuanqin Song
// cs201, sec (03), Feb 18, Exercise 3
package labs.lab4;

public class Potion {
    private String name;
    private double strength;

    public Potion() {
        this.name = "Defaut";
        this.strength = 0.0;
    }

    public Potion(String name, double strength) {
        this.name = name;
        this.strength = 0.0;
        setStrength(strength);
    }

    public String getName() {
        return this.name;
    }

    public double getStrength() {
        return this.strength;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setStrength(double strength) {
        if (validStrength(strength)) {
            this.strength = strength;
        }
    }

    public String toString() {
        return this.getName() + ", " + this.getStrength();
    }

    public Boolean validStrength(double strength) {
        return strength >= 0 && strength <= 10;
    }

    public Boolean equals(Potion potion) {
        return this.getName().equals(potion.getName()) && this.getStrength() == this.getStrength();
    }
}