// name: Yuanqin Song
// cs201, sec (03), Feb 18, Exercise 1

package labs.lab4;

public class GeoLocationClient {
    public static void main(String[] args) {
        GeoLocation gl = new GeoLocation();
        GeoLocation newYork = new GeoLocation(40.730610, -73.935242);


        // Display the values of the instance variables by calling the accessor methods.
        System.out.printf("the latitude of gl is %f, and the longitude is %f%n",gl.getLat(), gl.getLng());

        System.out.printf("the latitude of newYork is %f, and the longitude is %f%n",newYork.getLat(), newYork.getLng());

    }

}

