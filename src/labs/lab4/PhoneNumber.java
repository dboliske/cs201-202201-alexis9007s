// name: Yuanqin Song
// cs201, sec (03), Feb 18, Exercise 2

package labs.lab4;

public class PhoneNumber {
    private String countryCode;
    private String areaCode;
    private String number;

    public PhoneNumber() {
        this.countryCode = "000";
        this.areaCode = "000";
        this.number = "0000000";
    }

    public PhoneNumber(String countryCode, String areaCode, String number) {
        this.countryCode = countryCode;
        this.areaCode = "000";
        setAreaCode(areaCode);
        this.number = "0000000";
        setNumber(number);
    }

    public String getCountryCode() {
        return this.countryCode;
    }

    public String getAreaCode() {
        return this.areaCode;
    }

    public String getNumber() {
        return this.number;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public void setAreaCode(String areaCode) {
        if (validAreaCode(areaCode)) {
            this.areaCode = areaCode;
        }
    }

    public void setNumber(String number) {
        if (validNumber(number)) {
            this.number = number;
        }
    }

    public String toString() {
        return "+" + this.getCountryCode() + " " + this.getAreaCode() + " " + this.getNumber();
    }

    public Boolean validAreaCode(String areaCode) {
        return areaCode.length() == 3;
    }

    public Boolean validNumber(String number) {
        return  number.length() == 7;
    }

    public Boolean equals(PhoneNumber phoneNumber) {
        return this.getCountryCode().equals(phoneNumber.getCountryCode())  &&
                this.getAreaCode().equals(phoneNumber.getAreaCode())  &&
                this.getNumber().equals(phoneNumber.getNumber());
    }
}