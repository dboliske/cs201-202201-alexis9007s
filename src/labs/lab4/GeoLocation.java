// name: Yuanqin Song
// cs201, sec (03), Feb 18, Exercise 1

package labs.lab4;

public class GeoLocation {
    private double lat;
    private double lng;

    public GeoLocation() {
        this.lat = 0;
        this.lng = 0;
    }

    public GeoLocation(double lat, double lng) {
        this.lat = 0;
        setLat(lat);
        this.lng = 0;
        setLng(lng);
    }

    public double getLat() {
        return this.lat;
    }

    public double getLng() {
        return this.lng;
    }

    public void setLat(double lat) {
        if (validLat(lat)) {
            this.lat = lat;
        }
    }

    public void setLng(double lng) {
        if (validLng(lng)) {
            this.lng = lng;
        }
    }

    public String toString() {
        return "(" + this.lat + ", " + this.lng + ")";
    }

    public boolean validLat(double lat) {
        return lat <= 90 && lat >= -90;
    }

    public boolean validLng(double lng) {
        return lng <= 180 && lng >= -180;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof GeoLocation)) {
            return false;
        }
        GeoLocation geoLocation = (GeoLocation)obj;
        return this.getLat() == geoLocation.getLat() && this.getLng() == geoLocation.getLng();
    }

    public double calcDistance(GeoLocation gl) {
        double distance = Math.sqrt(Math.pow(this.lat-gl.getLat(), 2) + Math.pow(this.lng-gl.getLng(), 2));
        return distance;
    }

    public double calcDistance(double lat, double lng) {
        double distance = Math.sqrt(Math.pow(this.lat-lat, 2) + Math.pow(this.lng-lng, 2));
        return distance;
    }
}


