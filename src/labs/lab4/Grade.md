# Lab 4

## Total

-/30

## Break Down

* Part I GeoLocation
  * Exercise 1-9        4/8
  * Application Class   1/1
* Part II PhoneNumber
  * Exercise 1-9        4/8
  * Application Class   1/1
* Part III 
  * Exercise 1-8        4/8
  * Application Class   1/1
* Documentation         3/3

## Comments

1. Your classes should be separate files.
2. None of your non-default constructors or mutator methods do any validation before setting instance variables.
3. You did not follow the UML diagram when creating your validation methods.
