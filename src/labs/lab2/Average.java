// name: Yuanqin Song
// cs201, Lab2, Feb 2, Excercise 2

package labs.lab2;

import java.util.Scanner;

public class Average {

	public static void main(String[] args) {
		
		
		System.out.println("Input grades one by one, and enter -1 to finish entering grades.");
		Scanner input = new Scanner(System.in);
		
		double sum = 0;
		int count = 0;
		boolean finished = false;// initialise control flag
		
		do {
			System.out.print("Input a grade: ");
			String inputting = input.nextLine();
			
			if (inputting.equals("-1")) {
				System.out.println("You finished entering grades!");
				finished = true;
			} else {
				// check if input is a number
				try {
					count += 1;
					sum += Double.parseDouble(inputting);
				} catch (NumberFormatException e) {
					System.out.println("Invalid input! Please give a numeric input.");
					if (count > 0){
						count -= 1;
					}
				}
			}
		} while (!finished);
		
		double average = sum / count;
		
		input.close();
		
		System.out.println("Average grade is: " + average);
	}
}

