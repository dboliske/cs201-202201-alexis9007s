// name: Yuanqin Song
// cs201, Lab2, Feb 2, Excercise 1 

package labs.lab2;

import java.util.Scanner;

public class PrintSquare {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in); 
		System.out.print("input a dimension: "); //prompt user for a dimension
		
		int dimension = Integer.parseInt(input.nextLine()); 
		
		// use nested loop to implement the square
		for (int i=0; i<dimension; i++) {
			for(int j=0; j<dimension; j++) {
				System.out.print("* ");
			}
			System.out.println();
		}
		
		
		input.close();
	}

}
