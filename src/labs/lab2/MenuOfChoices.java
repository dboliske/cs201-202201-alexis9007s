// name: Yuanqin Song
// cs201, Lab2, Feb 2, Excercise 3

package labs.lab2;

import java.util.Scanner;

public class MenuOfChoices {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		boolean finished = false; // initialise the flag
		
		do {
			System.out.println();
			System.out.println("1: Say Hello - This should print \"Hello\" to console.");
			System.out.println("2: Addition - This should prompt the user to enter 2 numbers and return the sum of the two.");
			System.out.println("3: Multiplication - This should prompt the user to enter 2 numbers and return the product of the two.");
			System.out.println("4: Exit - Leave the program");
			System.out.print("Make choice from 1 to 4: "); // prompt user for a choice
			String choice = input.nextLine();
			
			switch (choice) {
				case "1":
					System.out.println("Hello");
					break;
					
				case "2":
					System.out.print("input the first number: ");
					double num1 = Double.parseDouble(input.nextLine());
					System.out.print("input the second number: ");
					double num2 = Double.parseDouble(input.nextLine());
					double sum = num1 + num2;
					System.out.println("The sum of the two is " + sum);
					break;
					
				case "3":
					System.out.print("input the first number: ");
					double multiNum1 = Double.parseDouble(input.nextLine());
					System.out.print("input the second number: ");
					double multiNum2 = Double.parseDouble(input.nextLine());
					double product = multiNum1 * multiNum2;
					System.out.println("The product of the two is " +  product);
					break;
					
				case "4":
					finished = true;
					break;
				default:
					System.out.println("Invalid input!");
			}
		} while (!finished);
		
		System.out.println("You exited the menu!");
		
		input.close();

	}

}
