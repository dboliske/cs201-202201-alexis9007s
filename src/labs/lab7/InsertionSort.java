// name: Yuanqin Song
// cs201, sec (03), Apr 3, Exercise 2

package labs.lab7;

public class InsertionSort {
    public static void main(String[] args) {
        String[] array = {"cat", "fat", "dog", "apple", "bat", "egg"};
        array = insertionSort(array);
        for (String s : array) {
            System.out.print(s + ", ");
        }
    }

    public static String[] insertionSort(String[] array) {
        for (int i=1; i< array.length; i++) {
            int j=i;
            while (j>0 && array[j].compareTo(array[j-1]) < 0) {
                String temp = array[j];
                array[j] = array[j-1];
                array[j-1] = temp;
                j--;
            }
        }
        return array;
    }
}
