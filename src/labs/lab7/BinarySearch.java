// name: Yuanqin Song
// cs201, sec (03), Apr 3, Exercise 4

package labs.lab7;

import java.util.Scanner;

public class BinarySearch {
    public static void main(String[] args) {
        String[] array = {"c", "html", "java", "python", "ruby", "scala"};

        Scanner input = new Scanner(System.in);
        System.out.print("Inuput searching value: ");
        String value = input.nextLine();

        int index = search(array, value);

        if (index == -1) {
            System.out.println(value + " not found.");
        } else {
            System.out.println(value + " found at index " + index + ".");
        }

        input.close();
    }

    public static int search(String[] array, String value) {
        int left = 0;
        int right = array.length-1;
        return binarySearch(array, left, right, value);
    }

    public static int binarySearch(String[] array, int left, int right, String value) {
        if (left > right) {
            return -1;
        }

        int mid = (left + right)/2;
        if (value.equalsIgnoreCase(array[mid])) {
            return mid;
        } else if (value.compareToIgnoreCase(array[mid]) > 0) {
            return binarySearch(array, mid+1, right, value);
        } else {
            return binarySearch(array, left, mid-1, value);
        }
    }
}
