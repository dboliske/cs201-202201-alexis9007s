// name: Yuanqin Song
// cs201, sec (03), Apr 3, Exercise 1

package labs.lab7;

import java.util.ArrayList;

public class BubbleSort {
    public static void main(String[] args) {
        int[] array = {10, 4, 7, 3, 8, 6, 1, 2, 5, 9};
        array = bubbleSort(array);
        for (int num: array) {
            System.out.print(num + ", ");
        }
    }

    public static int[] bubbleSort(int[] array) {
        boolean swapped = true;
        do {
            swapped = false;
            for (int i=0; i<array.length-1; i++) {
                if (array[i+1] < array[i]) {
                    int temp = array[i+1];
                    array[i+1] = array[i];
                    array[i] = temp;
                    swapped = true;
                }
            }
        } while (swapped);

        return array;
    }
}
