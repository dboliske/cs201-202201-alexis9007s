// name: Yuanqin Song
// cs201, sec (03), Apr 3, Exercise 3

package labs.lab7;

public class SelectionSort {
    public static void main(String[] args) {
        double[] array = {3.142, 2.718, 1.414, 1.732, 1.202, 1.618, 0.577, 1.304, 2.685, 1.282};
        array = selectionSort(array);
        for (double d : array) {
            System.out.print(d + ", ");
        }
    }

    public static double[] selectionSort(double[] array) {
        for (int i=0; i<array.length-1; i++) {
            int min = i;
            for (int j=i+1; j<array.length; j++) {
                if (array[j] < array[min]) {
                    min = j;
                }
            }
            if (min != i) {
                double temp = array[i];
                array[i] = array[min];
                array[min] = temp;
            }
        }
        return array;
    }
}
