// name: Yuanqin Song
// cs201, lab1, Jan.26, Exercise 2

package labs.lab1;

public class ArithmeticCalculations {

	public static void main(String[] args) {
		int myAge = 26;
		int fatherAge = 50;
		int birthYear = 1995;
		double height = 65.35; // height in inches

		int substract = fatherAge - myAge;
		int multiplyBirthyear = birthYear * 2;
		double convertToCms = height * 2.54;
		double convertToFeet = height * 0.0833333333;
		int convertToIntInches = (int) height;

		// age subtracted from father's age
		System.out.println("my age subtracted from father's age is: " + substract);
		// birth year multiplied by 2
		System.out.println("multiply birth year by 2 is: " + multiplyBirthyear);
		// convert height in inches to cms
		System.out.println("convert height in inches to cms: " + convertToCms + " cms");
		// convert height in inches to feet
		System.out.println("convert height in inches to feet: " + convertToFeet + " feet");
		// convert height in inches to integer
		System.out.println("convert height to integer inches: " + convertToIntInches + " inches");
		
	}

}
