// name: Yuanqin Song
// cs201, lab1, Jan.26, Exercise 3

package labs.lab1;

import java.util.Scanner;

public class DisplayFirstInitial {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		// prompt user for a first name
		System.out.print("input first name: ");
		
		// variable c represents the first initial
		char c = input.nextLine().charAt(0);
		
		input.close();
		
		System.out.println(c);
		

	}

}
