// name: Yuanqin Song
// cs201, lab1, Jan.26, Exercise 1

package labs.lab1;

import java.util.Scanner;

public class EchoName {

	public static void main(String[] args) {
		// create a variable for reading in user input
		Scanner input = new Scanner(System.in);
		
		// prompt user to input
		System.out.print("input a name: ");
		// read in user's input
		String name = input.nextLine();
		
		input.close();
		
		// print out the name
		System.out.println("Echo: " + name);
		
		
	}

}
