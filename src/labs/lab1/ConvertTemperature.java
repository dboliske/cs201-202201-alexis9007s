// name: Yuanqin Song
// cs201, lab1, Jan.27, Exercise 4

/*
 Fahrenheit TO Celsius
 
 since float point can not be compared with another float
 so we need to calculate the difference between expectation and actual result.
 diff = abs(expectation - actual result)
 if diff <= 0.0001, we regard it as passed.
 
 base on above conditon, all the test cases passed
 
test case  expectation           result
-40F         -40.00000C          -40.0C
-30F,        -34.44444C          -34.44444444444444C
-20F,        -28.88889C	         -28.88888888888889C
-10F,        -23.33333C          -23.333333333333332C
0F,          -17.77778C          -17.77777777777778C
10F,         -12.22222C          -12.222222222222221C
20F,         -6.666667c          -6.666666666666666C
100F         37.77778C           37.7777777777777C

Celsius TO Fahrenheit
test case  expectation      result
-40C         -40.00000F      -40.0F
-30C         -22.00000F	     -22.0F
-20C         -4.000000F	     -4.0F
-10C         14.00000F       14.0F
0C           32.00000F       32.0F
10C          50.00000F       50.0F
20C          68.00000F       68.0F
100C         212.00000F      212.0F
 */

package labs.lab1;

import java.util.Scanner;

public class ConvertTemperature {

	public static void main(String[] args) {
		
		
		Scanner input = new Scanner(System.in);
		
		//Prompt user for a temperature in Fahrenheit
		System.out.print("input a temperature in Fahrenheit: ");
		double inputFahrenheit = Double.parseDouble(input.nextLine());
		
		// convert the Fahrenheit to Celsius and display the result.
		double convertToCelsius = (inputFahrenheit - 32) / 1.8000 ;
		System.out.println(inputFahrenheit + " Fahrenheit = " + convertToCelsius + " Celsius");
		
		
		//Prompt the user for a temperature in Celsius
		System.out.print("input a temperature in Celsius: ");
		double inputCelsius = Double.parseDouble(input.nextLine());
		
		// convert the Celsius to Fahrenheit and display the result.
		double convertToFahrenheit = (inputCelsius * 1.8000) + 32;
		System.out.println(inputCelsius + " Celsius = " + convertToFahrenheit + " Fahrenheit");
		
				
		input.close();
		
	}

}
