// name: Yuanqin Song
// cs201, lab1, Jan.31, Exercise 6


/*
 diff = abs(expectation - actual result)
 if diff <= 0.0001, we regard it as passed
 all test cases below passed.
 
 inches     expectattion(centimeters)      result(centimeters)
  1              2.54                            2.54
  7              17.78                           17.78
  10             25.4                            25.4
  79             200.66                          200.66
  200            508                             508.0
  1000           2540                            2540.0
  
  all the cases above passed
 */
package labs.lab1;

import java.util.Scanner;

public class ConvertInchesToCentimeters {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		// prompt user for inches
		System.out.print("input inches: ");
		double inches = Double.parseDouble(input.nextLine());
		
		// convert inches to centimeters
		double centimeters = 2.54 * inches;
		
		System.out.println(inches + " inches = " + centimeters + " centimeters");
		
		input.close();
	}

}
