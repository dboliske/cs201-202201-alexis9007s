// name: Yuanqin Song
// cs201, lab1, Jan.26, Exercise 5



/*
 diff = abs(expectation - actual result)
 if diff <= 0.0001, we regard it as passed
 all test cases below passed.
 
 lenght,width,depth  expectation                   result
 
   0.04, 0.7, 1.2       1.8319999999999999         1.8319999999999999
   1, 3, 4              38.0                       38.0
   8, 14, 8             576.0                      576.0
   45, 55, 8.5          6650.0                     6650.0
   10, 100, 100         24000.0                    24000.0    


 */

package labs.lab1;

import java.util.Scanner;

public class SquareFeet {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		// prompt user to input length, width and depth
		System.out.print("type in length in inches: ");
		double length = Double.parseDouble(input.nextLine());
		
		
		
		System.out.print("type in width in inches: ");
		double width = Double.parseDouble(input.nextLine());
		
		System.out.print("type in depth in inches: ");
		double depth = Double.parseDouble(input.nextLine());
		
		double amount = ((length*width) + (length*depth) + (width*depth)) * 2;
		System.out.println(amount + " square feet of wood needed to make the box");
		
		input.close();
	}

}
