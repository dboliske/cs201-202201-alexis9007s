// name:  Yuanqin Song
// cs201, lab0,  Jan.24,  PrintSquare


package labs.lab0;

public class PrintSquare {

	public static void main(String[] args) {
		// output a size of 5-characters hollow square
		// firstly, draw the top side by print 5 chars, E.g."ccccc"
		// then draw three lines of "c   c" by using loop 
		// finally, draw the bottom side of the square as the first step.
		System.out.println("ccccc");
		for (int i = 0; i<3; i++) {
			System.out.println("c   c");
		}
		System.out.println("ccccc");
		
	}
}
