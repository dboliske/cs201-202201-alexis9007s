// name:  Yuanqin Song
// cs201, lab0,  Jan.24,  NameAndBirthay


package labs.lab0;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class NameAndBirthay {

	public static void main(String[] args) {
		String name = "Yuanqin Song";
		
		// initialise birth date 
		LocalDate birthday = LocalDate.of(1995,8,18);
		
		// use DateFormatter to format birth date 
		String formattedBirthday = birthday.format(DateTimeFormatter.ofPattern("MMM. dd, yyyy"));
		
		System.out.println("My name is " + name + " and my birthday is " + formattedBirthday);

	}

}
