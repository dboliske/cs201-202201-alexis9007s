// name: Yuanqin Song
// cs201, sec (03), Mar 4, CTAStation

package labs.lab5;

import labs.lab4.GeoLocation;

public class CTAStation extends GeoLocation {
    private String name;
    private String location;
    private boolean wheelChair;
    private boolean open;

    public CTAStation() {
        super();
        this.name = "";
        this.location = "";
        this.wheelChair = false;
        this.open = false;
    }

    public CTAStation(String name, double lat, double lng, String location, boolean wheelChair, boolean open) {
        super(lat, lng);
        this.name = name;
        this.location = location;
        this.wheelChair = wheelChair;
        this.open = open;
    }

    public String getName() {
        return name;
    }

    public String getLocation() {
        return location;
    }

    public boolean hasWheelChair() {
        return wheelChair;
    }

    public boolean isOpen() {
        return open;
    }

    public void setName(String name) {
        this.name = name;
    }
    public void setLocation(String location) {
        this.location = location;
    }

    public void setWheelChair(boolean wheelChair) {
        this.wheelChair = wheelChair;
    }

    public void setOpen(boolean open) {
        this.open = open;
    }

    @Override
    public String toString() {
        return name + "," + location + ","+ super.toString() + "," + wheelChair + "," + open  ;
    }

    @Override
    public boolean equals(Object obj) {
        if (!super.equals(obj)) {
            return false;
        }  else if (!(obj instanceof CTAStation)) {
            return false;
        }

        CTAStation station = (CTAStation)obj;

        if (!this.name.equals(station.name)) {
            return false;
        } else if (!this.location.equals(station.location)) {
            return false;
        } else if (this.hasWheelChair() != station.hasWheelChair()) {
            return false;
        } else if (this.isOpen() != station.isOpen()) {
            return false;
        }
        return true;
    }
}
