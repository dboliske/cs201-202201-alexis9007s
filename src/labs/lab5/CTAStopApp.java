// name: Yuanqin Song
// cs201, sec (03), Mar 4, CTAStopApp

package labs.lab5;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.util.Scanner;

public class CTAStopApp {

    public static void main(String[] args) {

        CTAStation[] data = readFile("src/labs/lab5/CTAStops.csv");//Load file
        menu(data); // pass data to menu()
    }

    public static CTAStation[] readFile(String filename) {
        CTAStation[] stations = new CTAStation[5];
        int count = 0;
        try {
            File f = new File(filename);
            Scanner input = new Scanner(f);
            input.nextLine();
            while (input.hasNextLine()) {
                String[] values = input.nextLine().split(",");
                try {
                    double lat = Double.parseDouble(values[1]);
                    double lng = Double.parseDouble(values[2]);
                    boolean wheelChair = Boolean.parseBoolean(values[4]);
                    boolean open = Boolean.parseBoolean(values[5]);

                    if (stations.length == count) {
                        stations = resize(stations, stations.length * 2);
                    }
                    stations[count] = new CTAStation(values[0], lat, lng, values[3], wheelChair, open);
                    count++;

                } catch (Exception e) {
                    System.out.println("Error happend when reading a line to CTAStops[], " + e);
                }
            }
            input.close();
        } catch (FileNotFoundException e) {
            System.out.println("File not found.");
        } catch (Exception e) {
            System.out.println("Reading in file error.");
        }
        stations = resize(stations, count);
        return stations;
    }

    public static CTAStation[] resize(CTAStation[] data, int size) {
        CTAStation[] temp = new CTAStation[size];
        int limit = data.length > size ? size : data.length;
        for (int i = 0; i < limit; i++) {
            temp[i] = data[i];
        }
        return temp;
    }

    public static void menu(CTAStation[] stations) {
        boolean done = false;
        Scanner input = new Scanner(System.in);
        do {
            System.out.println("1. Display Station Names");
            System.out.println("2. Display Stations with/without Wheelchair access");
            System.out.println("3. Display Nearest Station");
            System.out.println("4. Exit");
            System.out.print("Choice: ");

            String choice = input.nextLine();
            switch (choice) {
                case "1":
                    displayStationNames(stations);
                    break;
                case "2":
                    displayByWheelChair(input, stations);
                    break;
                case "3":
                    displayNearest(input, stations);
                    break;
                case "4":
                    done = true;
                    System.out.println("Goodbye!");
                    break;
                default:
                    System.out.println("Invalid input");
            }
        } while (!done);
        input.close();
    }

    public static void displayStationNames (CTAStation[] stations) {
        for (int i=0; i<stations.length; i++) {
            System.out.println(stations[i].getName());
        }
    }

    public static void displayByWheelChair(Scanner input, CTAStation[] stations) {
        boolean validChoice = false;
        do {
            System.out.print("Accessibility (y/n): ");
            String yn = input.nextLine();
            switch (yn) {
                case "y":
                    for (int i=0; i<stations.length; i++) {
                        if (stations[i].hasWheelChair()) {
                            System.out.println(stations[i]);
                        }
                    }
                    validChoice = true;
                    break;
                case "n":
                    for (int i=0; i<stations.length; i++) {
                        if (!stations[i].hasWheelChair()) {
                            System.out.println(stations[i]);
                        }
                    }
                    validChoice = true;
                    break;
                default:
                    System.out.println("Invalid choice, please enter y or n");
            }
        } while (!validChoice);
    }

    public static void displayNearest(Scanner input, CTAStation[] stations) {
        System.out.print("Enter a latitude: ");
        double lat = 0;
        try {
            lat = Double.parseDouble(input.nextLine());
        } catch (Exception e) {
            System.out.println("Invalid input.");
        }

        System.out.print("Enter a longitude: ");
        double lng = 0;
        try {
            lng = Double.parseDouble(input.nextLine());
        } catch (Exception e) {
            System.out.println("Invalid input.");
        }

        double nearest = stations[0].calcDistance(lat, lng);
        int stationIndex = 0;
        for (int i=0; i<stations.length; i++) {
            double dst = stations[i].calcDistance(lat,lng);
            if (dst < nearest) {
                nearest = dst;
                stationIndex = i;
            }
        }
        System.out.println("The nearest station is: " + stations[stationIndex].getName());
        System.out.println();
    }
}

