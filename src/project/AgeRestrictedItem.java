// Yuanqin Song, 18 Apr
// AgeRestrictedItem represents age restricted items

package project;

public class AgeRestrictedItem extends Item{
    private int age;

    public AgeRestrictedItem() {
        super();
        this.age = 18;
    }

    public AgeRestrictedItem(String name, double price, int age) {
        super(name, price);
        this.age = age;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        if (age >= 0) {
            this.age = age;
        }
    }

    @Override()
    public String toString() {
        return super.toString() + " Age restriction is " + age;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(super.equals(obj))) {
            return false;
        } else if(!(obj instanceof AgeRestrictedItem)) {
            return false;
        }

        AgeRestrictedItem ai = (AgeRestrictedItem)obj;
        if (this.age != ai.getAge()) {
            return false;
        }
        return true;
    }

    @Override
    public String csvData() {
        return "AgeRestrictedItem," + super.csvData() + "," + age;
    }
}


