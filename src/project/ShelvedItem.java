// Yuanqin Song, 18 Apr
// ShelvedItem class represents any item that are either shelf-stable food or non-food items

package project;

public class ShelvedItem extends Item {

    public ShelvedItem() {
        super();
    }

    public ShelvedItem(String name, double price) {
        super(name, price);
    }

    @Override
    public boolean equals(Object obj) {
        if (!(super.equals(obj))) {
            return false;
        } else if (!(obj instanceof ShelvedItem)) {
            return false;
        }
        return true;
    }

    @Override
    public String csvData() {
        return "ShelvedItem," + super.csvData();
    }
}
