// Yuanqin Song, 20 Apr
// StoreClient simulates the store.
// And my csv structure is like [type, name, price, expiration(age Restriction)]
// And in case there's any error occurred which causes file missing,
// I prepared a backup csv file which keeps the original data structure for recovering.

package project;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class StoreClient {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Load file: ");
        String filename = input.nextLine();

        // Load file
        ArrayList<Item> data = readFile(filename);
        // Menu
        data = menu(input, data);
        // Save file
        saveFile(filename, data);

        input.close();
        System.out.println("goodBye~");
    }

    public static ArrayList<Item> readFile(String filename) {
        ArrayList<Item> items = new ArrayList<Item>();

        try {
            File f = new File(filename);
            Scanner input = new Scanner(f);

            while (input.hasNextLine()) {
                try {
                    String line = input.nextLine();
                    String[] values = line.split(",");
                    Item item = null;
                    switch (values[0].toLowerCase()) {
                        case "produceitem":
                            item = new ProduceItem(
                                    values[1],
                                    Double.parseDouble(values[2]),
                                    values[3]);
                            break;
                        case "shelveditem":
                            item = new ShelvedItem(
                                    values[1],
                                    Double.parseDouble(values[2]));
                            break;
                        case "agerestricteditem":
                            item = new AgeRestrictedItem(
                                    values[1],
                                    Double.parseDouble(values[2]),
                                    Integer.parseInt(values[3]));
                            break;
                    }
                    items.add(item);
                } catch (Exception e) {
                    System.out.println("Error occurred in creating item.");
                }
            }
            input.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e) {
            System.out.println("Error occurred in reading file");
        }
        return items;
    }

    // Menu method displays operations we provide for users.
    public static ArrayList<Item> menu(Scanner input, ArrayList<Item> data) {
        boolean done = false;
        String[] options = { "Create and add new item", "Sell items", "Search an item", "Modify items", "Exit" };

        do {
            System.out.println();
            for (int i = 0; i < options.length; i++) {
                System.out.println((i + 1) + ". " + options[i]);
            }
            System.out.println("Enter choice: ");
            String choice = input.nextLine();

            switch (choice) {
                case "1":// Create and add new item
                    data = createItem(input, data);
                    break;
                case "2": // Sell items and remove them from the store
                    data = sellItem(input, data);
                    break;
                case "3": // Search for an item with a given name supplied by the user.
                    System.out.print("Search by entering a name: ");
                    String name = input.nextLine();
                    int pos = search(data, name);
                    if (pos != -1) {
                        System.out.println(name + " is found at postion " + (pos + 1));
                    } else {
                        System.out.println("Sorry " + name + " is not found.");
                    }
                    break;
                case "4": // Modify items
                    data = modifyItem(input, data);
                    break;
                case "5": // Exit
                    done = true;
                    break;
                default:
                    System.out.println("Invalid input.");
            }
        } while (!done);
        return data;
    }

    // CreateItem can help to create 3 different types of items.
    public static ArrayList<Item> createItem(Scanner input, ArrayList<Item> data) {
        Item item;
        System.out.println("Item name: ");
        String name = input.nextLine();
        System.out.println("Item price: ");
        double price;
        try {
            price = Double.parseDouble(input.nextLine());
        } catch (Exception e) {
            System.out.println("That is not a valid pirce.");
            return data;
        }

        System.out.println("Type of Item (ProduceItem, ShelvedItem or AgeRestrictedItem): ");
        String type = input.nextLine();
        switch (type.toLowerCase()) {
            case "produceitem":
                System.out.println("expiration date: ");
                String expiration = input.nextLine();
                item = new ProduceItem(name, price, expiration);
                break;
            case "shelveditem":
                item = new ShelvedItem(name, price);
                break;
            case "agerestricteditem":
                System.out.println("set an age restriction: ");
                int age = 0;
                try {
                    age = Integer.parseInt(input.nextLine());

                } catch (Exception e) {
                    System.out.println("Your input is not an integer.");
                }
                item = new AgeRestrictedItem(name, price, age);
                break;
            default:
                System.out.println("That is not a valid item type. Returning to menu.");
                return data;
        }
        data.add(item);
        return data;
    }

    // SellItem method firstly gets the item that user wants to add to cart by sellHelper method,
    // and then ckeck out by removing items in cart.
    public static ArrayList<Item> sellItem(Scanner input, ArrayList<Item> data) {
        ArrayList<Item> cart = new ArrayList<Item>();
        boolean checkout = false;

        do {
            Item item = sellHlper(input, data);
            if (item != null) {
                cart.add(item);
            }

            System.out.print("Add to cart successfully, go to check out? - (y/n): ");
            System.out.println();
            String yn = input.nextLine();
            if (yn.equalsIgnoreCase("y") || yn.equalsIgnoreCase("yes")) {
                checkout = true;
            } else if (yn.equalsIgnoreCase("n") || yn.equalsIgnoreCase("no")) {
                checkout = false;
            } else {
                System.out.println("That is not a valid input. Back to menu");
                return data;
            }

        } while (!checkout);

        // Check out
        if (checkout) {
            for (Item i : cart) {
                data.remove(i);
            }
        }
        System.out.println(cart.size() + " items have been sold.");

        return data;
    }

    // Filter an item to add to cart by a given item name.
    public static Item sellHlper(Scanner input, ArrayList<Item> data) {

        System.out.print("Choose an item to sell (by entering its name): ");
        String name = input.nextLine();

        boolean findItem = false;
        Item item = null;

        for (Item i : data) {
            if (findItem) {
                break;
            }
            if (name.equalsIgnoreCase(i.getName())) {
                findItem = true;
                item = i;
            }
        }
        if (!findItem) {
            System.out.println("Sorry, we do not have this item in stock.");
        }
        return item;

    }

    // Search an item by given name and return the position. If not found, it will return -1
    public static int search(ArrayList<Item> arr, String name) {
        for (int i = 0; i < arr.size(); i++) {
            if (arr.get(i).getName().equalsIgnoreCase(name)) {
                return i;
            }
        }
        return -1;
    }


    // Firstly, we need to know the item user wants to modify.
    // And then prompt user for attributes they want to change according to which type the item belongs to.
    public static ArrayList<Item> modifyItem(Scanner input, ArrayList<Item> data) {
        String[] prompts = {"1.Modify name, 2.Modify price", "1.Modify name, 2.Modify price, 3.Modify expirationDate", "1.Modify name, 2.Modify price, 3.Modify ageRestriction"};

        System.out.print("Enter the item you want to modify (e.g. pen or something): ");
        String target = input.nextLine();

        String type = ""; // find which type this "target" belongs to
        for (Item i : data) {
            if (i.getName().equalsIgnoreCase(target)) {
                type = i.getClass().getSimpleName();
                break;
            }
        }

        switch (type.toLowerCase()) {
            case "shelveditem":
                System.out.println(prompts[0]);
                break;
            case "produceitem":
                System.out.println(prompts[1]);
                break;

            case "agerestricteditem":
                System.out.println(prompts[2]);
                break;
            default:
                System.out.println("That is not a valid type. Back to menu.");
                return data;
        }

        System.out.println("Enter choice (1, 2 or 3): ");
        String choice = input.nextLine();
        switch (choice) {
            case "1":
                System.out.print("Enter a new name:");
                String newName = input.nextLine();
                for (Item item : data) {

                    if (item.getClass().getSimpleName().equalsIgnoreCase(type)
                            && item.getName().equalsIgnoreCase(target)) {
                        item.setName(newName);
                    }
                }
                break;
            case "2":
                System.out.print("Enter a new price:");
                try {
                    double newPrice = Double.parseDouble(input.nextLine());

                    for (Item item : data) {
                        if (item.getClass().getSimpleName().equalsIgnoreCase(type)
                                && item.getName().equalsIgnoreCase(target)) {
                            item.setPrice(newPrice);
                        }
                    }
                } catch (Exception e) {
                    System.out.println("That is not a valid price");
                }

                break;
            case "3":
                switch (type.toLowerCase()) {
                    case "shelveditem":
                        System.out.println("ShelvedItems do not have choice 3 function.");
                        break;
                    case "produceitem":
                        System.out.print("Enter a new expiration: ");
                        String newExpiration = input.nextLine();

                        for (Item item : data) {
                            if (item.getClass().getSimpleName().equalsIgnoreCase(type)
                                    && item.getName().equalsIgnoreCase(target)) {
                                ProduceItem pi = (ProduceItem) item;
                                pi.setExpirationDate(newExpiration);
                            }
                        }
                        break;
                    case "agerestricteditem":
                        System.out.print("Enter a new ageRestriction: ");
                        try {
                            int newAgeRestriction = Integer.parseInt(input.nextLine());

                            for (Item item : data) {
                                if (item.getClass().getSimpleName().equalsIgnoreCase(type)
                                        && item.getName().equalsIgnoreCase(target)) {
                                    AgeRestrictedItem ari = (AgeRestrictedItem) item;
                                    ari.setAge(newAgeRestriction);
                                }
                            }
                        } catch (Exception e) {
                            System.out.println("That is not a valid age format");
                        }
                        break;
                    default:
                        break;
                }
                break;
            default:
                System.out.println("That is not a valid choice.");
                break;
        }
        return data;
    }

    public static void saveFile(String filename, ArrayList<Item> data) {

        try {
            FileWriter writer = new FileWriter(filename);

            for (Item i : data) {
                writer.write(i.csvData() + "\n");
            }
            writer.flush();
            writer.close();
        } catch (Exception e) {
            System.out.println("Error to file: " + e.getMessage());
        }

    }

}
