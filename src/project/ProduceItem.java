// Yuanqin Song, 18 Apr
// ProduceItem class represents Produce items which have a expiration date

package project;

import javax.xml.crypto.Data;
import java.util.Date;

public class ProduceItem extends Item{
    private String expirationDate;

    public ProduceItem() {
        super();
        this.expirationDate = "01/01/2050";
    }

    public ProduceItem(String name, double price, String expirationDate) {
        super(name, price);
        this.expirationDate = expirationDate;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    @Override
    public String toString() {
        return super.toString() + " Expiration date is " + expirationDate;
    }

    @Override
    public boolean equals(Object obj) {
        if (!super.equals(obj)) {
            return false;
        } else if (!(obj instanceof ProduceItem)) {
            return false;
        }

        ProduceItem pi = (ProduceItem)obj;
        if(!this.expirationDate.equals(pi.getExpirationDate())) {
            return false;
        }
        return true;
    }

    @Override
    public String csvData() {
        return "ProduceItem," + super.csvData() + "," + expirationDate;
    }
}
