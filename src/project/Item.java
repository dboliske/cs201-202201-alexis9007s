// Yuanqin Song, 18 Apr
// Item class represents a general class

package project;

public class Item {
    private String name;
    private double price;

    public Item() {
        this.name = "none";
        this.price = 1.0;
    }

    public Item (String name, double price) {
        this.name = name;
        this.price = 1.0;
        setPrice(price);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        if (price > 0) {
            this.price = price;
        }
    }

    public String toString() {
        return name + " is " + price + " $.";
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof Item)) {
            return false;
        }

        Item i = (Item)obj;
        if (!this.name.equals(i.getName())) {
            return false;
        } else if (this.price != i.getPrice()) {
            return false;
        }
        return true;
    }

    public String csvData() {
        return name + "," + price;
    }


}
