# Final Project

## Total

136/150

## Break Down

Phase 1:                                                            45/50

- Description of the user interface                                 3/3
- Description of the programmer's tasks:
  - Describe how you will read the input                            3/3
  - Describe how you will process the data from the input file      4/4
  - Describe how you will store the data                            3/3
  - How will you add/delete/modify data?                            3/5
  - How will you search data?                                       5/5
- Classes: List of names and descriptions                           7/7
- UML Class Diagrams                                                5/10
- Testing Plan                                                      10/10

Phase 2:                                                            91/100

- Compiles and runs with no run-time errors                         10/10
- Documentation                                                     15/15
- Test plan                                                         10/10
- Inheritance relationship                                          5/5
- Association relationship                                          0/5
- Searching works                                                   5/5
- Uses a list                                                       5/5
- Project reads data from a file                                    5/5
- Project writes data to a file                                     5/5
- Project adds, deletes, and modifies data stored in list           3/5
- Project generates paths between any two stations                  10/10
- Project encapsulates data                                         5/5
- Project is well coded with good design                            8/10
- All classes are complete (getters, setters, toString, equals...)  5/5

## Comments

### Design Comments

- UML didn't represents association relationship in the program. -5

### Code Comments

- No association relationship in program. -5
- The implementations of adds, deletes and modifiers have something wrong. -2
- The project is not well coded. -2