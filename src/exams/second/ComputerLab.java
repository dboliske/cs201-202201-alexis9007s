// Yuanqin Song
// CS201,Question one

package exams.second;

public class ComputerLab extends Classroom{
    private boolean computers;

    public ComputerLab() {
        super();
        this.computers = false;
    }

    public ComputerLab(String building, String roomNumber, int seats, boolean computers) {
        super(building, roomNumber, seats);
        this.computers = computers;
    }

    public boolean isComputers() {
        return computers;
    }

    public void setComputers(boolean computers) {
        this.computers = computers;
    }

    @Override
    public String toString() {
        return super.toString() + (computers? "Equipped with computers":"Does not equipped with computers ");
    }

}
