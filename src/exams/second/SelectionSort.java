// Yuanqin Song
// CS201,Question Four

package exams.second;

public class SelectionSort {
    public static void main(String[] args) {

        String[] array = {"speaker", "poem", "passenger", "tale", "reflection", "leader", "quality", "percentage", "height", "wealth", "resource", "lake", "importance"};
        array = selectionSort(array);
        for (String s : array) {
            System.out.print(s + ", ");
        }
    }

    public static String[] selectionSort(String[] array) {
        for (int i=0; i<array.length-1; i++) {
            int min = i;
            for (int j=i+1; j<array.length; j++) {

                if (array[j].compareToIgnoreCase(array[min]) < 0) {
                    min = j;
                }
            }
            if (min != i) {
                String temp = array[i];
                array[i] = array[min];
                array[min] = temp;
            }
        }
        return array;
    }
}
