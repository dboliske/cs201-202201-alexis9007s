// Yuanqin Song
// CS201,Question one

package exams.second;

public class Classroom {
    protected String building;
    protected String roomNumber;
    private int seats;

    public Classroom() {
        this.building = "Com1";
        this.roomNumber = "room1";
        this.seats = 1;
    }

    public Classroom(String building, String roomNumber, int seats) {
        this.building = building;
        this.roomNumber = roomNumber;
        this.seats = 1;
        setSeats(seats);
    }

    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    public String getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(String roomNumber) {
        this.roomNumber = roomNumber;
    }

    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        if (seats > 0) {
            this.seats = seats;
        }
    }

    public String toString() {
        return building + "-" + roomNumber + " has " + seats + " seats. ";
    }
}
