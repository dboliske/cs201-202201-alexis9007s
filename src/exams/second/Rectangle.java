// Yuanqin Song
// CS201,Question Two

package exams.second;

public class Rectangle extends Polygon{

    private double width;
    private double height;

    public Rectangle() {
        this.width = 1;
        this.height = 2;
        setName("rectangle");
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getWidth() {
        return width;
    }

    public double getHeight() {
        return height;
    }

    @Override
    public String toString() {
        return super.toString() +
                "Rectangle{" +
                "width=" + width +
                ", height=" + height +
                '}';
    }

    @Override
    public double area() {
        double a = this.width * this.height;
        return a;
    }

    @Override
    public double perimeter() {
        double p = 2.0 * (height + width);
        return p;
    }
}
