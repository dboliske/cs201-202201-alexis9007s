// Yuanqin Song
// CS201,Question Five

package exams.second;

import java.util.Scanner;

public class JumpSearch {
    public static void main(String[] args) {
        double[] nums = {0.577, 1.202, 1.282, 1.304, 1.414, 1.618, 1.732, 2.685, 2.718, 3.142};

        Scanner input = new Scanner(System.in);
        System.out.print("Search term: ");
        Double value = null;
        try {
            value = Double.parseDouble(input.nextLine());
        } catch (Exception e) {
            System.out.println("Error occurred in parsing Double: " + e);
        }

        int index = search(nums, value);
        if (index == -1) {
            System.out.println("-1, not found");
        } else {
            System.out.println(value + " found at index " + index + ".");
        }

        input.close();
    }

    public static int search(double[] array, double value) {
        int step = (int) Math.sqrt(array.length);
        int curPosition = step-1;
        return jumpSearch(array, value, step, curPosition);
    }


    public static int jumpSearch(double[] array, double value, int step, int curPosition) {
        if (curPosition < array.length && value > array[curPosition]) {
            curPosition += step;
            return jumpSearch(array, value, step, curPosition);
        } else {
            for (int start=(curPosition - step + 1); start <= curPosition && start < array.length; start++) {
                if (value == array[start]) {
                    return start;
                }
            }
        }

        return -1; // Value not found
    }
}
