package exams.second;

// This client aims to test Question 1 and Question 2

public class Client {
    public static void main(String[] args) {
        Classroom c = new Classroom("office building", "room2", -10);
        ComputerLab lab = new ComputerLab("computer building", "lab1", 5, true);

        //System.out.println(c.toString());
        //System.out.println(lab.toString());


        Rectangle rectangle = new Rectangle();

        System.out.println(rectangle.name);
        System.out.println(rectangle.toString());
        System.out.println(rectangle.area());
        System.out.println(rectangle.perimeter());

        Circle circle = new Circle();

        System.out.println(circle.name);
        System.out.println(circle.toString());
        System.out.println(circle.area());
        System.out.println(circle.perimeter());

    }
}
