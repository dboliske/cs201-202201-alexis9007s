# Final Exam

## Total

99/100

## Break Down

1. Inheritance/Polymorphism:    20/20
    - Superclass:               5/5
    - Subclass:                 5/5
    - Variables:                5/5
    - Methods:                  5/5
2. Abstract Classes:            20/20
    - Superclass:               5/5
    - Subclasses:               5/5
    - Variables:                5/5
    - Methods:                  5/5
3. ArrayLists:                  19/20
    - Compiles:                 5/5
    - ArrayList:                5/5
    - Exits:                    5/5
    - Results:                  4/5
4. Sorting Algorithms:          20/20
    - Compiles:                 4/5
    - Selection Sort:           10/10
    - Results:                  5/5
5. Searching Algorithms:        20/20
    - Compiles:                 5/5
    - Jump Search:              10/10
    - Results:                  5/5

## Comments

1. ok
2. ok
3. Error when the first input is "done".-1
4. ok
5. ok