// Yuanqin Song
// CS201,Question Three

package exams.second;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Scanner;

public class MinAndMax {
    public static void main(String[] args) {
        ArrayList<Double> result = findMinAndMax();
        System.out.println("Minimum number is: " + result.get(0) + " Maxmum number is: " + result.get(1));
    }

    public static ArrayList<Double> findMinAndMax() {
        Scanner input = new Scanner(System.in);
        boolean done = false;
        ArrayList<Double> nums = new ArrayList<Double>();
        do {
            System.out.print("Enter a number: ");
            String value = input.nextLine();

            if (value.equalsIgnoreCase("Done")) {
                done = true;
            } else {
                Double num;
                try {
                    num = Double.parseDouble(value);
                    nums.add(num);
                } catch (Exception e) {
                    System.out.println("Error occurred when parsing Double: " + e);
                }

            }

        } while(!done);

        input.close();

        Double min = Collections.min(nums);
        Double max = Collections.max(nums);
        ArrayList<Double> result = new ArrayList<Double>();

        result.add(min);
        result.add(max);

        return result;
    }


}
