// Yuanqin Song
// CS201,Question Two

package exams.second;

public class Circle extends Polygon{
    private double radius;

    public Circle() {
        super();
        this.radius = 1;
        setName("circle");
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    @Override
    public String toString() {
        return super.toString() +
                "Circle{" +
                "radius=" + radius +
                '}';
    }

    @Override
    public double area() {
        double a = Math.PI * radius * radius;
        return a;
    }

    @Override
    public double perimeter() {
        double p = 2.0 * Math.PI * radius;
        return p;
    }
}

