package exams.first;

public class PetClient {
    public static void main(String[] args) {
        Pet p1 = new Pet();
        Pet p2 = new Pet("Bubble", 11);

        System.out.println(p1.toString());
        System.out.println(p2.toString());
        System.out.println(p1.equals(p2));
        p2.setAge(2);
        System.out.println(p2.toString());
        p2.setAge(-1);
        System.out.println(p2.toString());
    }

}
