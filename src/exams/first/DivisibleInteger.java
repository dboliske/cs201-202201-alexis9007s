// name: Yuanqin Song
// cs201, sec (03), Feb 26, Question 2: Selection

package exams.first;

import java.util.Scanner;

public class DivisibleInteger {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter an integer: ");

        try {
            int n = Integer.parseInt(input.nextLine());

            if (n % 2 == 0 && n % 3 != 0) {
                System.out.println("foo");
            } else if (n % 3 == 0 && n % 2 != 0) {
                System.out.println("bar");
            } else if (n % 2 == 0 && n % 3 == 0) {
                System.out.println("foobar");
            }

        } catch (Exception e) {
            System.out.println("Invalid inputting, please enter an integer");
        }

        input.close();
    }
}
