// name: Yuanqin Song
// cs201, sec (03), Feb 26, Question 3: Repetition

package exams.first;

import java.util.Scanner;

public class Triangle {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter an integer: ");

        try {
            int n = Integer.parseInt(input.nextLine());

            for (int i=0; i<n; i++) {
                for (int j=0; j<n; j++) {
                    if (j < i) {
                        System.out.print("  ");
                    } else {
                        System.out.print("* ");
                    }
                }
                System.out.println();
            }
        } catch (Exception e) {
            System.out.println("Invalid inputting, please enter an integer");
        }

        input.close();
    }
}
