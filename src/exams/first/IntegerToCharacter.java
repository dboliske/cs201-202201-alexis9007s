// name: Yuanqin Song
// cs201, sec (03), Feb 26, Question 1: Data Types

package exams.first;

import java.util.Scanner;

public class IntegerToCharacter {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Input an integer: ");

        try {
            String line = input.nextLine();
            int n = Integer.parseInt(line);
            char c = (char) (n+65);
            System.out.println(c);
        } catch (Exception e) {
            System.out.println("Input is not valid, please enter an Integer.");
        }

        input.close();
    }
}
