// name: Yuanqin Song
// cs201, sec (03), Feb 26, Question 4: Arrays

package exams.first;

import java.util.Scanner;

public class Words {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String[] words = new String[5];

        for (int i=0; i < words.length; i++) {
            System.out.print("Enter a word: ");
            words[i] = input.nextLine();
        }

        // Count occurrences of words
        int[] count = {1, 1, 1, 1, 1};

        for (int i=0; i < words.length; i++ ) {
            for (int j=i+1; j < words.length; j++) {
                if (words[i].equals(words[j]) && count[j] > 0) {
                    count[i]++; // merge the count for the same word
                    count[j]--; // as count[j] has been added to count[i], count[j] itself should be reduced by one.
                }
            }
        }

        // print out words appearing more than once
        for (int i=0; i < count.length; i++) {
            if (count[i] > 1) {
                System.out.println(words[i] + " appears more than once.");
            }
        }

        input.close();
    }
}
