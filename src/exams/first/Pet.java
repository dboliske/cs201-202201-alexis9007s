// name: Yuanqin Song
// cs201, sec (03), Feb 26, Question 5: Objects

package exams.first;

public class Pet {
    private String name;
    private int age;

    public Pet() {
        this.name = "Lucky";
        this.age = 1;
    }

    public Pet(String name, int age) {
        this.name = name;
        this.age = 1;
        setAge(age);
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        if (age >= 0) {
            this.age = age;
        }
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof Pet)) {
            return false;
        } else {
            Pet p = (Pet)obj;
            if (!this.name.equals(p.getName())) {
                return false;
            } else if (this.getAge() != p.getAge()) {
                return false;
            }
            return true;
        }
    }

    public String toString() {
        return this.getName() + " is " + this.getAge() + " years old.";
    }
}
